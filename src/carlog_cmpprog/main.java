package carlog_cmpprog;

 
import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.CardLayout;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;

 
import javax.swing.ButtonGroup;
 
 
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;


import javax.swing.JPasswordField;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.*;
import javax.swing.JOptionPane;

import java.sql.*;
public class main {

	static final String JDBC_DRIVER = "org.sqlite.JDBC";  
	static final String DB_URL = "jdbc:sqlite:C:\\Users\\Artem-PC\\Desktop\\CarLogRepo\\carlog_repoprog\\carlog.db";
	Connection conn =null;
	Statement stmt =null; 

	private JFrame frame;
	private JPanel login_panel;
	private JTextField login_textField;
	private JPasswordField log_passwordField;
	private JButton btnEntrar;
	private JLabel lblCriarUtilizador;
	private JPanel createusr_panel;
	private JLabel lblNome_1;
	private JLabel lblApelido;
	private JLabel lblEmail;
	private JLabel lblSexo;
	private JLabel lblIdade;
	private JTextField createusr_textField;
	private JTextField textField_2;
	private JRadioButton rdbtnMasculino;
 
	private JRadioButton rdbtnFeminino;
 
	private JRadioButton rdbtnNewRadioButton;
 
	private JComboBox comboBox;
	private JLabel lblCofirmarPasse;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JButton btnCriar;
	private JButton btnVoltar;
	private JPanel user_panel;
	private JList listuser;
	private DefaultListModel<carro> listModel;
	private JPanel carro_panel;
	private JTextField txtMarca;
	private JTextField txtModelo;
	private JTextField txtCor;
	private JTextField txtCilindrada;
	private JTextField txtAnoAq;
	private JTextField txtPrecoAq;
	private JTextField txtAnodeVenda;
	private JTextField txtPrecoVenda;
	private JTextField txtMatricula;
	private JList<carro> listuser_1;
	 
	 
 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main window = new main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main() {
		initialize();
	}


	private void connectDB(){
		try{
			 
				Class.forName(JDBC_DRIVER);
				System.out.println("Connecting to database...");
				conn = DriverManager.getConnection(DB_URL);
				stmt = conn.createStatement();
				System.out.println("Quering database...");
			}catch(Exception e){
				System.out.println(e.getMessage());
			}

	}
	/**
	 * Initialize the contents of the frame.
	 */
	
	private void initialize() {
		
		connectDB();
		frame = new JFrame();
		frame.setBounds(100, 100, 448, 486);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));

		/* JPanel ola_panel = new JPanel();
		frame.getContentPane().add(ola_panel, "name_63364906693526");
		GridBagLayout gbl_ola_panel = new GridBagLayout();
		gbl_ola_panel.columnWidths = new int[]{101, 0, 0, 0};
		gbl_ola_panel.rowHeights = new int[]{50, 0, 30, 0, 51, 0, 0};
		gbl_ola_panel.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_ola_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		ola_panel.setLayout(gbl_ola_panel);

		JLabel lblOl = new JLabel("Ol\u00E1 ! ! !");
		GridBagConstraints gbc_lblOl = new GridBagConstraints();
		gbc_lblOl.insets = new Insets(0, 0, 5, 0);
		gbc_lblOl.gridx = 2;
		gbc_lblOl.gridy = 1;
		ola_panel.add(lblOl, gbc_lblOl);

		JLabel lblObrigadoPorEscolher = new JLabel("Obrigado por escolher a nossa aplica\u00E7\u00E3o ! ! !");
		GridBagConstraints gbc_lblObrigadoPorEscolher = new GridBagConstraints();
		gbc_lblObrigadoPorEscolher.insets = new Insets(0, 0, 5, 0);
		gbc_lblObrigadoPorEscolher.gridx = 2;
		gbc_lblObrigadoPorEscolher.gridy = 3;
		ola_panel.add(lblObrigadoPorEscolher, gbc_lblObrigadoPorEscolher);

		JButton btnComear = new JButton("Come\u00E7ar >");
		btnComear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ola_panel.setVisible(false);
				login_panel.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnComear = new GridBagConstraints();
		gbc_btnComear.gridx = 2;
		gbc_btnComear.gridy = 5;
		ola_panel.add(btnComear, gbc_btnComear);








 
		 
 
		login_panel = new JPanel();
		frame.getContentPane().add(login_panel, "name_63376942405179");
		GridBagLayout gbl_login_panel = new GridBagLayout();
		gbl_login_panel.columnWidths = new int[]{78, 0, 0, 46, 116, 0, 0, 0};
		gbl_login_panel.rowHeights = new int[]{0, 69, 64, 0, 30, 33, 0};
		gbl_login_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_login_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		login_panel.setLayout(gbl_login_panel);

		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Yu Gothic UI", Font.PLAIN, 13));
		GridBagConstraints gbc_lblNome = new GridBagConstraints();
		gbc_lblNome.insets = new Insets(0, 0, 5, 5);
		gbc_lblNome.gridx = 2;
		gbc_lblNome.gridy = 2;
		login_panel.add(lblNome, gbc_lblNome);

		login_textField = new JTextField();
		GridBagConstraints gbc_login_textField = new GridBagConstraints();
		gbc_login_textField.insets = new Insets(0, 0, 5, 5);
		gbc_login_textField.gridx = 4;
		gbc_login_textField.gridy = 2;
		login_panel.add(login_textField, gbc_login_textField);
		login_textField.setColumns(10);

		JLabel lblPalavrapasse = new JLabel("Palavra-Passe");
		lblPalavrapasse.setFont(new Font("Yu Gothic UI", Font.PLAIN, 13));
		GridBagConstraints gbc_lblPalavrapasse = new GridBagConstraints();
		gbc_lblPalavrapasse.insets = new Insets(0, 0, 5, 5);
		gbc_lblPalavrapasse.gridx = 2;
		gbc_lblPalavrapasse.gridy = 3;
		login_panel.add(lblPalavrapasse, gbc_lblPalavrapasse);

		log_passwordField = new JPasswordField();
		log_passwordField.setColumns(10);
		GridBagConstraints gbc_log_passwordField = new GridBagConstraints();
		gbc_log_passwordField.insets = new Insets(0, 0, 5, 5);
		gbc_log_passwordField.gridx = 4;
		gbc_log_passwordField.gridy = 3;
		login_panel.add(log_passwordField, gbc_log_passwordField);

		lblCriarUtilizador = new JLabel("Criar Utilizador");
		lblCriarUtilizador.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				login_panel.setVisible(false);
				createusr_panel.setVisible(true);
			}
		});
		GridBagConstraints gbc_lblCriarUtilizador = new GridBagConstraints();
		gbc_lblCriarUtilizador.insets = new Insets(0, 0, 0, 5);
		gbc_lblCriarUtilizador.gridx = 2;
		gbc_lblCriarUtilizador.gridy = 5;
		login_panel.add(lblCriarUtilizador, gbc_lblCriarUtilizador);

		btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//LOGIN///////////////////////LOGIN////////LOGIN/////
				try{
					String nome = login_textField.getText().trim();
					String pass = log_passwordField.getText().trim();
					String sql = "Select * FROM utilizador WHERE nome = '" + nome + "' and pass = '"+ pass +"'";
					 ResultSet  rs=stmt.executeQuery(sql);
					 int cont = 0;
					 
					 while(rs.next()){
						 cont=cont+1; 
					 }
					 if(cont==1){
						 JOptionPane.showMessageDialog(null, "Bem-Vindo,  "+nome+""); 
							user_panel.setVisible(true);
							login_panel.setVisible(false);
					 }
					 else if(cont>1){ 
						 JOptionPane.showMessageDialog(null, "Utilizador Duplicado"); 
					 }
					 else if(login_textField.getText().isEmpty() || log_passwordField.getText().isEmpty()){ 
						 JOptionPane.showMessageDialog(null, "Deve preencher os campos"); 
					 }
					 else{ 
						 JOptionPane.showMessageDialog(null, "Utilizador n�o existe"); 
					 } 
				}catch(Exception e){
					 
				}	 
					//////////////////////////FIM///////////////////////////////
				
				
			}
		});
		 
		
		GridBagConstraints gbc_btnEntrar = new GridBagConstraints();
		gbc_btnEntrar.insets = new Insets(0, 0, 0, 5);
		gbc_btnEntrar.gridx = 4;
		gbc_btnEntrar.gridy = 5;
		login_panel.add(btnEntrar, gbc_btnEntrar);

		createusr_panel = new JPanel();
		frame.getContentPane().add(createusr_panel, "name_156358914702487");
		GridBagLayout gbl_createusr_panel = new GridBagLayout();
		gbl_createusr_panel.columnWidths = new int[]{63, 0, 30, 20, 90, 27, 0, 0, 0};
		gbl_createusr_panel.rowHeights = new int[]{23, 26, 32, 40, 28, 35, 0, 48, 0, 0};
		gbl_createusr_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_createusr_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		createusr_panel.setLayout(gbl_createusr_panel);

		lblNome_1 = new JLabel("Nome");
		GridBagConstraints gbc_lblNome_1 = new GridBagConstraints();
		gbc_lblNome_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNome_1.gridx = 1;
		gbc_lblNome_1.gridy = 1;
		createusr_panel.add(lblNome_1, gbc_lblNome_1);

		createusr_textField = new JTextField();
		GridBagConstraints gbc_createusr_textField = new GridBagConstraints();
		gbc_createusr_textField.gridwidth = 2;
		gbc_createusr_textField.insets = new Insets(0, 0, 5, 5);
		gbc_createusr_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_createusr_textField.gridx = 4;
		gbc_createusr_textField.gridy = 1;
		createusr_panel.add(createusr_textField, gbc_createusr_textField);
		createusr_textField.setColumns(10);

		lblApelido = new JLabel("Palavra - Passe");
		GridBagConstraints gbc_lblApelido = new GridBagConstraints();
		gbc_lblApelido.insets = new Insets(0, 0, 5, 5);
		gbc_lblApelido.gridx = 1;
		gbc_lblApelido.gridy = 2;
		createusr_panel.add(lblApelido, gbc_lblApelido);

		passwordField_1 = new JPasswordField();
		GridBagConstraints gbc_passwordField_1 = new GridBagConstraints();
		gbc_passwordField_1.gridwidth = 2;
		gbc_passwordField_1.insets = new Insets(0, 0, 5, 5);
		gbc_passwordField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField_1.gridx = 4;
		gbc_passwordField_1.gridy = 2;
		createusr_panel.add(passwordField_1, gbc_passwordField_1);

		lblCofirmarPasse = new JLabel("Cofirmar Passe");
		GridBagConstraints gbc_lblCofirmarPasse = new GridBagConstraints();
		gbc_lblCofirmarPasse.insets = new Insets(0, 0, 5, 5);
		gbc_lblCofirmarPasse.gridx = 1;
		gbc_lblCofirmarPasse.gridy = 3;
		createusr_panel.add(lblCofirmarPasse, gbc_lblCofirmarPasse);

		passwordField = new JPasswordField();
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.gridwidth = 2;
		gbc_passwordField.insets = new Insets(0, 0, 5, 5);
		gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField.gridx = 4;
		gbc_passwordField.gridy = 3;
		createusr_panel.add(passwordField, gbc_passwordField);

		lblEmail = new JLabel("Email");
		GridBagConstraints gbc_lblEmail = new GridBagConstraints();
		gbc_lblEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmail.gridx = 1;
		gbc_lblEmail.gridy = 4;
		createusr_panel.add(lblEmail, gbc_lblEmail);

		textField_2 = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.gridwidth = 2;
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 4;
		gbc_textField_2.gridy = 4;
		createusr_panel.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);

		lblSexo = new JLabel("Sexo");
		GridBagConstraints gbc_lblSexo = new GridBagConstraints();
		gbc_lblSexo.insets = new Insets(0, 0, 5, 5);
		gbc_lblSexo.gridx = 1;
		gbc_lblSexo.gridy = 5;
		createusr_panel.add(lblSexo, gbc_lblSexo);

 
		rdbtnMasculino = new JRadioButton("Masculino",true);
		 
 
 
 
		GridBagConstraints gbc_rdbtnMasculino = new GridBagConstraints();
		gbc_rdbtnMasculino.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnMasculino.gridx = 4;
		gbc_rdbtnMasculino.gridy = 5;
		createusr_panel.add(rdbtnMasculino, gbc_rdbtnMasculino);

 
		rdbtnFeminino = new JRadioButton("Feminino");
		GridBagConstraints gbc_rdbtnFeminino = new GridBagConstraints();
		gbc_rdbtnFeminino.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnFeminino.gridx = 6;
		gbc_rdbtnFeminino.gridy = 5;
		createusr_panel.add(rdbtnFeminino, gbc_rdbtnFeminino);

		 ButtonGroup group = new ButtonGroup();
		  group.add(rdbtnMasculino);
		    group.add(rdbtnFeminino);

		
		
 
		rdbtnNewRadioButton = new JRadioButton("Feminino");
		GridBagConstraints gbc_rdbtnNewRadioButton = new GridBagConstraints();
		gbc_rdbtnNewRadioButton.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnNewRadioButton.gridx = 6;
		gbc_rdbtnNewRadioButton.gridy = 5;
		createusr_panel.add(rdbtnNewRadioButton, gbc_rdbtnNewRadioButton);

 
		lblIdade = new JLabel("Idade");
		GridBagConstraints gbc_lblIdade = new GridBagConstraints();
		gbc_lblIdade.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdade.gridx = 1;
		gbc_lblIdade.gridy = 6;
		createusr_panel.add(lblIdade, gbc_lblIdade);

		comboBox = new JComboBox<Integer>();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.gridwidth = 2;
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 4;
		gbc_comboBox.gridy = 6;
		createusr_panel.add(comboBox, gbc_comboBox);
		
		for (int i = 18; i <= 100; i++) {
			 
		    comboBox.addItem(new Integer(i));
 
		   
		}

		btnCriar = new JButton("Criar");

 
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				///////////////////////////////////////CRIAR UTILIZADOR////////////////////////////
				
				
 
				
				String valuesOfCheckBox="";
		        
				
				if(rdbtnMasculino.isSelected())
		        {
		        	 
		            valuesOfCheckBox+=rdbtnMasculino.getText()+" ";
		            
		        }
		        else if(rdbtnFeminino.isSelected())
		        {
		            valuesOfCheckBox+=rdbtnFeminino.getText()+" ";
		        }
		       

				String nome = createusr_textField.getText().trim();
				String pass = passwordField_1.getText().trim();
				String confpass = passwordField.getText().trim();
				String email = textField_2.getText().trim();
 
				String sexo =  valuesOfCheckBox;
				String idade = String.valueOf(comboBox.getSelectedItem().toString());
				
				
				
				if(createusr_textField.getText().trim().equals("") || passwordField_1.getText().trim().equals("") || passwordField.getText().trim().equals("") || textField_2.getText().trim().equals("")  ) {
					JOptionPane.showMessageDialog(null, "N�o pode deixar os campos vazios"); 
				} 	
				else if(pass.equals(confpass)){
				String sql = "INSERT INTO utilizador(nome,pass,confpass,email,sexo,idade) VALUES" + " ('"+nome+"',  '"+pass+"', '"+confpass+"', '"+email+ "', '"+sexo+"', '"+idade+"');";
				JOptionPane.showMessageDialog(null, "Utilizador criado com sucesso"); 
 
			
				System.out.println(sql);
				try{
				stmt.executeUpdate(sql); 
				}				
				catch(SQLException e1){
					 
				e1.printStackTrace();
					
				}
 
				
				createusr_panel.setVisible(false);
				login_panel.setVisible(true);
				}
			else {
			 JOptionPane.showMessageDialog(null, "A palavra - passe de confirma��o n�o coincide"); 
			}
				
				
 
 
				}
		});
		GridBagConstraints gbc_btnCriar = new GridBagConstraints();
		gbc_btnCriar.insets = new Insets(0, 0, 0, 5);
		gbc_btnCriar.gridx = 1;
		gbc_btnCriar.gridy = 8;
		createusr_panel.add(btnCriar, gbc_btnCriar);

		btnVoltar = new JButton("Voltar");
		btnVoltar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				login_panel.setVisible(true);
				createusr_panel.setVisible(false);
			}
		});
		GridBagConstraints gbc_btnVoltar = new GridBagConstraints();
		gbc_btnVoltar.insets = new Insets(0, 0, 0, 5);
		gbc_btnVoltar.gridx = 4;
		gbc_btnVoltar.gridy = 8;
		createusr_panel.add(btnVoltar, gbc_btnVoltar);
 */
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		user_panel = new JPanel();
		frame.getContentPane().add(user_panel, "name_66680091371041");
		user_panel.setLayout(null);
		listModel = new DefaultListModel<carro>(); 
		fillList("SELECT * FROM carro");
		listuser = new JList<carro>(listModel);
		
		listuser_1 = new JList<carro>(listModel);
 
		listuser_1.setBounds(47, 98, 373, 134);
		user_panel.add(listuser_1);
		
		JLabel lblNome_2 = new JLabel("Nome");
		lblNome_2.setBounds(42, 29, 46, 14);
		user_panel.add(lblNome_2);
		
		JLabel lblNewLabel = new JLabel("Meus Carros");
		lblNewLabel.setBounds(273, 29, 78, 14);
		user_panel.add(lblNewLabel);
		
		JButton btnEditarPerfil = new JButton("Editar Perfil");
		btnEditarPerfil.setBounds(47, 256, 115, 23);
		user_panel.add(btnEditarPerfil);
		
		JButton btnAdicionarCarro = new JButton("Adicionar Carro");
		btnAdicionarCarro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				carro_panel.setVisible(true);
				user_panel.setVisible(false);
			}
		});
		btnAdicionarCarro.setBounds(248, 256, 146, 23);
		user_panel.add(btnAdicionarCarro);
		
		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				login_panel.setVisible(true);
				user_panel.setVisible(false);
			}
		});
		btnSair.setBounds(162, 338, 89, 23);
		user_panel.add(btnSair);
  
	  
		  
		
		
		
		
		
		
		
		
		
		
		 
		
	 
		listuser_1.setBounds(47, 98, 373, 68);
		user_panel.add(listuser_1);
		
		carro_panel = new JPanel();
		frame.getContentPane().add(carro_panel, "name_286889503542836");
		carro_panel.setLayout(null);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(50, 38, 46, 14);
		carro_panel.add(lblMarca);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(193, 38, 46, 14);
		carro_panel.add(lblModelo);
		
		JLabel lblCor = new JLabel("Cor");
		lblCor.setBounds(39, 148, 90, 14);
		carro_panel.add(lblCor);
		
		JLabel lblCilindrada = new JLabel("Cilindrada");
		lblCilindrada.setBounds(318, 148, 90, 14);
		carro_panel.add(lblCilindrada);
		
		JLabel lblCombustivel = new JLabel("Combustivel");
		lblCombustivel.setBounds(173, 148, 110, 14);
		carro_panel.add(lblCombustivel);
		
		JLabel lblAnoDeAquisio = new JLabel("Ano de Aquisi\u00E7\u00E3o");
		lblAnoDeAquisio.setBounds(39, 236, 110, 14);
		carro_panel.add(lblAnoDeAquisio);
		
		JLabel lblPreoDeAquisio = new JLabel("Pre\u00E7o de Aquisi\u00E7\u00E3o");
		lblPreoDeAquisio.setBounds(261, 236, 110, 14);
		carro_panel.add(lblPreoDeAquisio);
		
		txtMarca = new JTextField();
		txtMarca.setBounds(24, 63, 86, 20);
		carro_panel.add(txtMarca);
		txtMarca.setColumns(10);
		
		txtModelo = new JTextField();
		txtModelo.setBounds(173, 63, 86, 20);
		carro_panel.add(txtModelo);
		txtModelo.setColumns(10);
		
		txtCor = new JTextField();
		txtCor.setBounds(10, 173, 86, 20);
		carro_panel.add(txtCor);
		txtCor.setColumns(10);
		
		JComboBox comboCombustivel = new JComboBox();
		comboCombustivel.setBounds(168, 173, 86, 20);
		carro_panel.add(comboCombustivel);
		comboCombustivel.addItem("Gasolina");
		comboCombustivel.addItem("Diesel");
		comboCombustivel.addItem("Biodiesel");
		comboCombustivel.addItem("GPL");
		
		
		
		txtCilindrada = new JTextField();
		txtCilindrada.setBounds(305, 173, 86, 20);
		carro_panel.add(txtCilindrada);
		txtCilindrada.setColumns(10);
		
		txtAnoAq = new JTextField();
		txtAnoAq.setBounds(39, 261, 86, 20);
		carro_panel.add(txtAnoAq);
		txtAnoAq.setColumns(10);
		
		txtPrecoAq = new JTextField();
		txtPrecoAq.setBounds(261, 261, 86, 20);
		carro_panel.add(txtPrecoAq);
		txtPrecoAq.setColumns(10);
		
		JLabel lblAnoDeVenda = new JLabel("Ano de Venda");
		lblAnoDeVenda.setBounds(50, 309, 79, 14);
		carro_panel.add(lblAnoDeVenda);
		
		JLabel lblPreoDeVenda = new JLabel("Pre\u00E7o de Venda");
		lblPreoDeVenda.setBounds(261, 309, 88, 14);
		carro_panel.add(lblPreoDeVenda);
		
		txtAnodeVenda = new JTextField();
		txtAnodeVenda.setBounds(39, 334, 86, 20);
		carro_panel.add(txtAnodeVenda);
		txtAnodeVenda.setColumns(10);
		
		txtPrecoVenda = new JTextField();
		txtPrecoVenda.setBounds(261, 334, 86, 20);
		carro_panel.add(txtPrecoVenda);
		txtPrecoVenda.setColumns(10);
		
		JButton btnCriarCarro = new JButton("Criar");
		btnCriarCarro.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				/////////////////////////////CRIAR CARRO//////////////////////////////////////////
				
				

				
			 

				String marca = txtMarca.getText().trim();
				String modelo = txtModelo.getText().trim();
				String cor = txtCor.getText().trim();
				String cilindrada = txtCilindrada.getText().trim();
				String matricula = txtMatricula.getText().trim();	
				String ano_aquisicao = txtAnoAq.getText().trim();
				String preco_aquisicao = txtPrecoAq.getText().trim();
				String preco_venda = txtPrecoVenda.getText().trim();
				String ano_venda = txtAnodeVenda.getText().trim();
				String combustivel = String.valueOf(comboCombustivel.getSelectedItem().toString());
				
				if(txtMarca.getText().trim().equals("") || txtModelo.getText().trim().equals("") || txtCor.getText().trim().equals("") || txtCilindrada.getText().trim().equals("") 
					|| txtMatricula.getText().trim().equals("") || txtAnoAq.getText().trim().equals("") || txtPrecoAq.getText().trim().equals("") || txtPrecoVenda.getText().trim().equals("")|| txtAnodeVenda.getText().trim().equals("") ) {
					JOptionPane.showMessageDialog(null, "N�o pode deixar os campos vazios"); 
				} 	
				else{
				String sql = "INSERT INTO carro(marca,modelo,cor,cilindrada,matricula,ano_aquisicao,preco_aquisicao,preco_venda,ano_venda,combustivel) VALUES" + 
				" ('"+marca+"',  '"+modelo+"', '"+cor+"', '"+cilindrada+ "', '"+matricula+"', '"+ano_aquisicao+"', '"+preco_aquisicao+"', '"+preco_venda+"', '"+ano_venda+"', '"+combustivel+"');";
				 
				
				System.out.println(sql);
				try{
				stmt.executeUpdate(sql); 
				}				
				catch(SQLException e1){
					 
				e1.printStackTrace();
					
				}
				}
				
				
				
				
				
				
				
				
				
				////////////////////////////////////////////////////////////////
			}
		});
		btnCriarCarro.setBounds(60, 399, 89, 23);
		carro_panel.add(btnCriarCarro);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(261, 399, 89, 23);
		carro_panel.add(btnCancelar);
		
		JLabel lblMatricula = new JLabel("Matricula");
		lblMatricula.setBounds(318, 38, 73, 14);
		carro_panel.add(lblMatricula);
		
		txtMatricula = new JTextField();
		txtMatricula.setBounds(305, 63, 86, 20);
		carro_panel.add(txtMatricula);
		txtMatricula.setColumns(10);
 
		
		 
	}

	private void fillList(String sql){
		try{
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
				carro cl = new carro(
						 rs.getString("Marca"),
						rs.getString("Modelo"),	
						rs.getString("Cor"),
						rs.getString("Cilindrada"),
						rs.getString("Combustivel"),
						rs.getString("Matricula"),
						rs.getInt ("Ano_Aquisicao"),
						rs.getInt ("Preco_Aquisicao"),
						rs.getInt ("Preco_Venda"),
						rs.getInt ("Ano_Venda"));
				
				listModel.addElement(cl);
			};

			}catch(Exception e){
				System.out.println(e.getMessage());

			}
		}

	protected JButton getBtnWegfgsd() {
		return getBtnWegfgsd();
	}
	protected JPanel getLogin_panel() {
		return login_panel;
	}
	public JList getListuser() {
		return listuser_1;
	}
}


package carlog_cmpprog;
 
public class cliente {
 
	private String Nome; 
	private int Idade;
	private String Sexo;
	private String Email;
	private String Passe;
	
	public String getPasse() {
		return Passe;
	}

	public void setPasse(String passe) {
		Passe = passe;
	}

	public int getIdade() {
		return Idade;
	}

	public void setIdade(int idade) {
		Idade = idade;
	}

	public String getSexo() {
		return Sexo;
	}

	public void setSexo(String sexo) {
		Sexo = sexo;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public cliente( String nome, int idade, String sexo, String email) {
		super();
		 
		Nome = nome;
		Idade = idade;
		Sexo = sexo;
		Email = email;
	}

	
	 

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	

	@Override
	public String toString() {
		return "[Nome=" + Nome + ", Idade=" + Idade + ", Sexo=" + Sexo + ", Email=" + Email + "]";
	}
	
}

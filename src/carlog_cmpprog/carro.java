
package carlog_cmpprog;
 
public class carro {
 
	private String Marca; 
	private String Modelo;
	private String Cor;
	private String Cilindrada;
	private String Matricula;
	private String Combustivel;
	private int Ano_Aquisicao;
	private int Preco_Aquisicao;
	private int Preco_Venda;
	private int Ano_Venda;
	
	
	public String getMarca() {
		return Marca;
	}
	public String getModelo() {
		return Modelo;
	}
	public String getCor() {
		return Cor;
	}
	public String getCilindrada() {
		return Cilindrada;
	}
	public String getMatricula() {
		return Matricula;
	}
	public String getCombustivel() {
		return Combustivel;
	}
	public int getAno_Aquisicao() {
		return Ano_Aquisicao;
	}
	public int getPreco_Aquisicao() {
		return Preco_Aquisicao;
	}
	public int getPreco_Venda() {
		return Preco_Venda;
	}
	public int getAno_Venda() {
		return Ano_Venda;
	}
	public void setMarca(String marca) {
		Marca = marca;
	}
	public void setModelo(String modelo) {
		Modelo = modelo;
	}
	public void setCor(String cor) {
		Cor = cor;
	}
	public void setCilindrada(String cilindrada) {
		Cilindrada = cilindrada;
	}
	public void setMatricula(String matricula) {
		Matricula = matricula;
	}
	public void setCombustivel(String combustivel) {
		Combustivel = combustivel;
	}
	public void setAno_Aquisicao(int ano_Aquisicao) {
		Ano_Aquisicao = ano_Aquisicao;
	}
	public void setPreco_Aquisicao(int preco_Aquisicao) {
		Preco_Aquisicao = preco_Aquisicao;
	}
	public void setPreco_Venda(int preco_Venda) {
		Preco_Venda = preco_Venda;
	}
	public void setAno_Venda(int ano_Venda) {
		Ano_Venda = ano_Venda;
	}
	
	/*public carro( String marca, String modelo, String cor, String cilindrada, String matricula, String combustivel, int ano_aquisicao , int preco_aquisicao , int preco_venda,  int ano_venda ) {
		super();
									 
									Marca = marca;
									Modelo = modelo;
									Cor = cor;  
									Cilindrada = cilindrada;  
									Matricula=matricula;
									Ano_Aquisicao= ano_aquisicao;
									Preco_Aquisicao=preco_aquisicao;
									Preco_Venda=preco_venda; 
									Ano_Venda=ano_venda;
									Combustivel= combustivel;
	}*/
	public carro(String marca, String modelo, String cor, String cilindrada, String matricula, String combustivel, int ano_aquisicao , int preco_aquisicao , int preco_venda,  int ano_venda) {
		// TODO Auto-generated constructor stub
		Marca = marca;
	 
		Modelo = modelo;
		Cor = cor;  
		Cilindrada = cilindrada;  
		Matricula=matricula;
		Ano_Aquisicao= ano_aquisicao;
		Preco_Aquisicao=preco_aquisicao;
		Preco_Venda=preco_venda; 
		Ano_Venda=ano_venda;
		Combustivel= combustivel;
		
		
	}
	@Override
	public String toString() {
		return "carro [Marca=" + Marca + ", Modelo=" + Modelo + ", Cor=" + Cor + ", Cilindrada=" + Cilindrada
				+ ", Matricula=" + Matricula + ", Combustivel=" + Combustivel + ", Ano_Aquisicao=" + Ano_Aquisicao
				+ ", Preco_Aquisicao=" + Preco_Aquisicao + ", Preco_Venda=" + Preco_Venda + ", Ano_Venda=" + Ano_Venda
				+ "]";
	}
	
	
}